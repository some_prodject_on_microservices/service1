package app

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/service1/models"
)

type Usecase interface {
	AcceptNewTicket(ctx context.Context, newTicket models.Ticket) (string, error)
	GetTicketList(ctx context.Context, userID string) ([]models.Ticket, error)
}
