package app

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/service1/models"
)

// Repository ...
type Repository interface {
	CreateTicket(ctx context.Context, ticket models.Ticket) error
	GetTicketList(ctx context.Context, userID string) ([]models.Ticket, error)
}
