package grpc

import (
	"context"

	pb "gitlab.com/some_prodject_on_microservices/api/service1"
	"gitlab.com/some_prodject_on_microservices/service1/app"
	"gitlab.com/some_prodject_on_microservices/service1/models"
)

type Handler struct {
	uc app.Usecase
	pb.UnimplementedService1Server
}

func NewHandler(uc app.Usecase) *Handler {
	return &Handler{
		uc: uc,
	}
}

func (h *Handler) NewTicket(ctx context.Context, newTicket *pb.Ticket) (*pb.TicketID, error) {
	mTicket := models.Ticket{
		User: models.User{
			ID:    newTicket.User.ID,
			Email: newTicket.User.Email,
		},
	}

	id, err := h.uc.AcceptNewTicket(ctx, mTicket)
	if err != nil {
		return nil, err
	}

	return &pb.TicketID{ID: id}, nil
}
