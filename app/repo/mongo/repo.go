package mongo

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/service1/models"
	"gitlab.com/some_prodject_on_microservices/service1/app"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	ticketCollection = "tickets"
)

type mongoRepo struct {
	ticketCollection *mongo.Collection
}

func NewMongoRepo(db *mongo.Database) app.Repository {
	return &mongoRepo{
		ticketCollection: db.Collection(ticketCollection),
	}
}

func (r *mongoRepo) CreateTicket(ctx context.Context, ticket models.Ticket) error {
	_, err := r.ticketCollection.InsertOne(ctx, r.toDbTicket(ticket))
	return err
}

func (r *mongoRepo) GetTicketList(ctx context.Context, userID string) ([]models.Ticket, error) {
	var (
		dbTickets []dbTicket      = make([]dbTicket, 0)
		mTicekts  []models.Ticket = make([]models.Ticket, 0)
	)

	cur, err := r.ticketCollection.Find(ctx, bson.M{"user_id": userID})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var ticket dbTicket

		err := cur.Decode(&ticket)
		if err != nil {
			return nil, err
		}

		dbTickets = append(dbTickets, ticket)
	}

	for _, ticket := range dbTickets {
		mTicekts = append(mTicekts, r.toModelTicket(ticket))
	}

	return mTicekts, nil
}
