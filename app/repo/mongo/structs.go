package mongo

import (
	"github.com/google/uuid"
	"gitlab.com/some_prodject_on_microservices/service1/models"
)

type dbTicket struct {
	ID     string `bson:"_id"`
	User   dbUser `bson:"user"`
	Result bool   `bson:"result"`
	Status int    `bson:"status"`
}

func (r *mongoRepo) toDbTicket(ticket models.Ticket) dbTicket {
	return dbTicket{
		ID:     ticket.ID.String(),
		User:   r.toDbUser(ticket.User),
		Result: ticket.Result,
		Status: int(ticket.Status),
	}
}

func (r *mongoRepo) toModelTicket(ticket dbTicket) models.Ticket {
	return models.Ticket{
		ID:     uuid.MustParse(ticket.ID),
		User:   r.toModelUser(ticket.User),
		Result: ticket.Result,
		Status: models.TicketStatus(ticket.Status),
	}
}

type dbUser struct {
	ID    string `bson:"_id"`
	Email string `bson:"email"`
}

func (r *mongoRepo) toDbUser(user models.User) dbUser {
	return dbUser{
		ID:    user.ID,
		Email: user.Email,
	}
}

func (r *mongoRepo) toModelUser(user dbUser) models.User {
	return models.User{
		ID:    user.ID,
		Email: user.Email,
	}
}
