package usecase

import (
	"context"
	"encoding/json"

	"github.com/DarkSoul94/services"
	queueclient "github.com/DarkSoul94/services/pkg/QueueClient"
	"github.com/google/uuid"
	"gitlab.com/some_prodject_on_microservices/service1/app"
	"gitlab.com/some_prodject_on_microservices/service1/models"
)

type usecase struct {
	qCli queueclient.QueueClient
	repo app.Repository
}

func NewUsecase(cli queueclient.QueueClient, r app.Repository) app.Usecase {
	return &usecase{
		qCli: cli,
		repo: r,
	}
}

func (u *usecase) AcceptNewTicket(ctx context.Context, newTicket models.Ticket) (string, error) {
	newTicket.ID = uuid.New()
	newTicket.Status = models.Accept

	err := u.repo.CreateTicket(ctx, newTicket)
	if err != nil {
		return "", err
	}

	data, err := json.Marshal(newTicket)
	if err != nil {
		return "", err
	}

	err = u.qCli.Publish(services.NewTicketQueueName, data)
	if err != nil {
		return "", err
	}

	return newTicket.ID.String(), nil
}

func (u *usecase) GetTicketList(ctx context.Context, userID string) ([]models.Ticket, error) {
	return u.repo.GetTicketList(ctx, userID)
}
