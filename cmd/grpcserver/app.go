package grpcserver

import (
	"context"
	"fmt"
	"net"

	pb "gitlab.com/some_prodject_on_microservices/api/service1"

	mongodb "github.com/DarkSoul94/dbconnectors/MongoDB"
	"github.com/streadway/amqp"
	"gitlab.com/some_prodject_on_microservices/service1/app"
	"gitlab.com/some_prodject_on_microservices/service1/pkg/QueueClient/rabbitmq"

	appgrpc "gitlab.com/some_prodject_on_microservices/service1/app/delivery/grpc"
	apprepo "gitlab.com/some_prodject_on_microservices/service1/app/repo/mongo"
	appusecase "gitlab.com/some_prodject_on_microservices/service1/app/usecase"

	"gitlab.com/some_prodject_on_microservices/service1/config"

	"google.golang.org/grpc"
)

type Deps struct {
	Service1Handler pb.Service1Server
}

type App struct {
	Deps

	Conf       config.Config
	rabbitConn *amqp.Connection
	rabbitChan *amqp.Channel
	Repo       app.Repository
	grpcServer *grpc.Server
}

func NewApp(conf config.Config) *App {
	db, err := mongodb.MongoDbInit(context.Background(), conf.DbHost, conf.DbPort, conf.DbName)
	if err != nil {
		panic(err)
	}

	repo := apprepo.NewMongoRepo(db)

	con, chn, err := rabbitmq.Connect(
		conf.RabbitLogin,
		conf.RabbitPass,
		conf.RabbitHost,
		conf.RabbitPort,
	)
	if err != nil {
		panic(err)
	}

	publisher := rabbitmq.NewRabbitClient(chn)

	uc := appusecase.NewUsecase(publisher, repo)

	return &App{
		Deps: Deps{
			Service1Handler: appgrpc.NewHandler(uc),
		},
		rabbitConn: con,
		rabbitChan: chn,
		grpcServer: grpc.NewServer(),
		Conf:       conf,
		Repo:       repo,
	}
}

func (a *App) Run() {
	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", a.Conf.GrpcPort))
	if err != nil {
		panic(err)
	}

	pb.RegisterService1Server(a.grpcServer, a.Service1Handler)

	a.grpcServer.Serve(l)
}

func (a *App) Stop() {
	a.grpcServer.Stop()
}
