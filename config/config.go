package config

import (
	"github.com/spf13/viper"
)

type Config struct {
	Release     bool
	AppName     string
	GrpcPort    string
	LogDir      string
	LogFile     string
	DbHost      string
	DbPort      string
	DbName      string
	RabbitLogin string
	RabbitPass  string
	RabbitHost  string
	RabbitPort  string
}

// InitConfig - load config from config.yml
func InitConfig() Config {
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	conf := Config{
		Release:  viper.GetBool("release"),
		AppName:  viper.GetString("name"),
		GrpcPort: viper.GetString("grpc_port"),
		LogDir:   viper.GetString("log.dir"),
		LogFile:  viper.GetString("log.file"),
		DbHost:   viper.GetString("db.host"),
		DbPort:   viper.GetString("db.port"),
		DbName:   viper.GetString("db.name"),
		RabbitLogin: viper.GetString("rabbit.login"),
		RabbitPass: viper.GetString("rabbit.pass"),
		RabbitHost: viper.GetString("rabbit.host"),
		RabbitPort: viper.GetString("rabbit.port"),
	}

	return conf
}
